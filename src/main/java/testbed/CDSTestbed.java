package testbed;

import concurrent.delayed.delayed_map.IDelayedMap;
import concurrent.delayed.delayed_map.multiple_owner.DelayedMultipleOwnerConcurrentHashMap;

import java.util.Map;
import java.util.Set;

public class CDSTestbed
{
    public static void main(String[] arg)
    {
        IDelayedMap<String, String> delayedMap = new DelayedMultipleOwnerConcurrentHashMap<>();

        delayedMap.queueKVChange("bob", "bobaroo");
        delayedMap.queueKVChange("sally", "sallaroo");

        delayedMap.executeKVChanges();

        Map<String, String> map = delayedMap.getMapUnsafe();

        Set<Map.Entry<String, String>> entrySet = map.entrySet();

        for(Map.Entry<String, String> entry : entrySet)
        {
            System.out.println(entry.getKey() + " : " + entry.getValue());
        }
    }
}
