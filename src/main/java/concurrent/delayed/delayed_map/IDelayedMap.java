package concurrent.delayed.delayed_map;

import java.util.Map;

public interface IDelayedMap<K,V> extends IDelayedWriteOnlyMap<K,V>
{
    void executeKVChanges();

    Map<K, V> getMapUnsafe();
}
