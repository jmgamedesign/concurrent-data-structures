package concurrent.delayed.delayed_map;

import concurrent.dump_bucket.IDumpBucket;
import concurrent.dump_bucket.SynchronizedDumpBucket;

public abstract class DelayedMap<K, V> implements IDelayedMap<K, V>
{
    protected final IDumpBucket<DelayedMapTuple<K,V>> changeQueue = new SynchronizedDumpBucket<>();

    @Override
    public boolean queueKVChange(K _k, V _v)
    {
        if(_v == null)
        {
            throw new NullPointerException("Null _v argument passed into DelayedMap's queueKVChange() method.");
        }

        DelayedMapTuple<K, V> tuple = new DelayedMapTuple<>(_k, _v);

        return changeQueue.add(tuple);
    }
}
