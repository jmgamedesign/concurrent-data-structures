package concurrent.delayed.delayed_map;

public interface IDelayedWriteOnlyMap<K,V>
{
    boolean queueKVChange(K _k, V _v);
}
