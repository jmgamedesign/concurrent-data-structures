package concurrent.delayed.delayed_map;

public class DelayedMapTuple<K,V>
{
    public final K k;
    public final V v;

    DelayedMapTuple(K _k, V _v)
    {
        k = _k;
        v = _v;
    }
}
