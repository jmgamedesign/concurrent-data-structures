package concurrent.delayed.delayed_map.single_owner;

import concurrent.delayed.delayed_map.DelayedMap;
import concurrent.delayed.delayed_map.DelayedMapTuple;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class DelayedSingleOwnerHashMap<K, V> extends DelayedMap<K, V>
{
    private final Map<K, V> hashMap = new HashMap<>();

    @Override
    public void executeKVChanges()
    {
        Collection<DelayedMapTuple<K, V>> tuples = changeQueue.dump();

        for(DelayedMapTuple<K, V> tuple : tuples)
        {
            hashMap.put(tuple.k, tuple.v);
        }
    }

    @Override
    public Map<K, V> getMapUnsafe()
    {
        return hashMap;
    }
}
