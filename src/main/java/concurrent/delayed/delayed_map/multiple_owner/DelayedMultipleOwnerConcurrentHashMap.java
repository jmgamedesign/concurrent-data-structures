package concurrent.delayed.delayed_map.multiple_owner;

import concurrent.delayed.delayed_map.DelayedMap;
import concurrent.delayed.delayed_map.DelayedMapTuple;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class DelayedMultipleOwnerConcurrentHashMap<K, V> extends DelayedMap<K, V>
{
    private final ConcurrentMap<K, V> concurrentHashMap = new ConcurrentHashMap<>();

    @Override
    public void executeKVChanges()
    {
        Collection<DelayedMapTuple<K, V>> tuples = changeQueue.dump();

        for(DelayedMapTuple<K, V> tuple : tuples)
        {
            concurrentHashMap.put(tuple.k, tuple.v);
        }
    }

    @Override
    public ConcurrentMap<K, V> getMapUnsafe()
    {
        return concurrentHashMap;
    }
}
