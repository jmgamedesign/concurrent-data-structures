package concurrent.delayed.delayed_collection.single_owner;

import concurrent.delayed.delayed_collection.DelayedCollection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class DelayedSingleOwnerArrayList<E> extends DelayedCollection<E>
{
    private final List<E> arrayList = new ArrayList<>();

    @Override
    public void executeWrites()
    {
        arrayList.addAll(addQueue.dump());
        arrayList.removeAll(removeQueue.dump());
    }

    @Override
    public List<E> getCollectionUnsafe()
    {
        return arrayList;
    }
}
