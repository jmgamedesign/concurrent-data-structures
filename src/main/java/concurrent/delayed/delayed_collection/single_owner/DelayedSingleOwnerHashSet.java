package concurrent.delayed.delayed_collection.single_owner;

import concurrent.delayed.delayed_collection.DelayedCollection;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class DelayedSingleOwnerHashSet<E> extends DelayedCollection<E>
{
    private final Set<E> hashSet = new HashSet<>();

    @Override
    public void executeWrites()
    {
        hashSet.addAll(addQueue.dump());
        hashSet.removeAll(removeQueue.dump());
    }

    @Override
    public Set<E> getCollectionUnsafe()
    {
        return hashSet;
    }
}
