package concurrent.delayed.delayed_collection.multiple_owner;

import concurrent.delayed.delayed_collection.DelayedCollection;

import java.util.Collection;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class DelayedMultipleOwnerConcurrentLinkedQueue<E> extends DelayedCollection<E>
{
    private final Queue<E> concurrentLinkedQueue = new ConcurrentLinkedQueue<>();

    @Override
    public void executeWrites()
    {
        concurrentLinkedQueue.addAll(addQueue.dump());
        concurrentLinkedQueue.removeAll(removeQueue.dump());
    }

    @Override
    public Queue<E> getCollectionUnsafe()
    {
        return concurrentLinkedQueue;
    }
}
