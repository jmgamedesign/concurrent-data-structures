package concurrent.delayed.delayed_collection;

import java.util.Collection;

public interface IDelayedCollection<E> extends IDelayedWriteOnlyCollection<E>
{
    void executeWrites();

    Collection<E> getCollectionUnsafe();
}
