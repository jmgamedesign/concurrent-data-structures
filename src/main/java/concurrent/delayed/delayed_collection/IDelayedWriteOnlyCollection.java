package concurrent.delayed.delayed_collection;

public interface IDelayedWriteOnlyCollection<E>
{
    boolean queueAddingElement(E _e);

    boolean queueRemovingElement(E _e);
}
