package concurrent.delayed.delayed_collection;

import concurrent.dump_bucket.IDumpBucket;
import concurrent.dump_bucket.SynchronizedDumpBucket;

public abstract class DelayedCollection<E> implements IDelayedCollection<E>
{
    protected final IDumpBucket<E> addQueue = new SynchronizedDumpBucket<>();
    protected final IDumpBucket<E> removeQueue = new SynchronizedDumpBucket<>();

    @Override
    public boolean queueAddingElement(E _e)
    {
        if(_e == null)
        {
            throw new NullPointerException("Null _e argument passed into DelayedCollection's queueAddingElements() method.");
        }

        return addQueue.add(_e);
    }

    @Override
    public boolean queueRemovingElement(E _e)
    {
        if(_e == null)
        {
            throw new NullPointerException("Null _e argument passed into DelayedCollection's queueRemovingElements() method.");
        }

        return removeQueue.add(_e);
    }
}
