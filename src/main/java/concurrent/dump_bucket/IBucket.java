package concurrent.dump_bucket;

public interface IBucket<E>
{
    boolean add(E _e);
}
