package concurrent.dump_bucket;

import java.util.Collection;

public interface IDumpBucket<E> extends IBucket<E>
{
    Collection<E> dump();
}
