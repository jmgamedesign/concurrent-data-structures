package concurrent.dump_bucket;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class SynchronizedDumpBucket<E> implements IDumpBucket<E>
{
    private volatile Queue<E> concurrentLinkedQueue = new ConcurrentLinkedQueue<>();

    public synchronized boolean add(E _e)
    {
        return concurrentLinkedQueue.add(_e);
    }

    public synchronized Queue<E> dump()
    {
        Queue<E> current = concurrentLinkedQueue;
        concurrentLinkedQueue = new ConcurrentLinkedQueue<>();

        return current;
    }
}
